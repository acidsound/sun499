'use strict';

/*
 * 카테고리 목록
 * name : 카테고리명
 * img : 카테고리 대표 이미지의 URL
 */
var home = [
  { name:'Gigbags', img:'/img/goods/index-gigbags.jpg' },
  { name:'Pedalboards', img:'/img/goods/index-pedalboards.jpg' },
  { name:'Straps', img:'/img/goods/index-straps.jpg' },
];