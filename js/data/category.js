'use strict';

/*
 * 카테고리별 목록
 * name : 카테고리명
 * img : 카테고리 대표 이미지의 URL
 */

var category = {
  'Gigbags' : [
    { name : 'Electric Gigbag', img : '/img/goods/S_EG.jpg'},
    { name : 'Acoustic Gigbag', img : '/img/goods/S_AGDN.jpg'},
    { name : 'Bass Gigbag', img : '/img/goods/S_BG.jpg'},
    { name : 'Pedalboard Gigbag Junior', img : '/img/goods/S_BB_JR.jpg'},
    { name : 'Pedalboard Gigbag Standard', img : '/img/goods/S_BB_STD.jpg'}
  ],
  'Pedalboards' : [
    { name : 'Pedalboard Junior', img : '/img/goods/S_B_JR.jpg'},
    { name : 'Pedalboard Standard', img : '/img/goods/S_B_STD.jpg'}
  ],
  'Straps' : [
    { name : '50 Gold1', img : '/img/goods/PS_50BG1B.jpg'},
    { name : '50 Gold2', img : '/img/goods/PS_50BG2B.jpg'},
    { name : '50 Silver1', img : '/img/goods/PS_50BS1B.jpg'},
    { name : '50 Silver2', img : '/img/goods/PS_50BS2B.jpg'},
    { name : 'Gray Scale', img : '/img/goods/PS_50BGYB.jpg'},
    { name : 'Black Net', img : '/img/goods/PS_50RBB.jpg'},
    { name : 'Black Stripe', img : '/img/goods/PS_50BRBB.jpg'},
    { name : 'White Stripe', img : '/img/goods/PS_50RWRW.jpg'},
    { name : 'Red Stripe', img : '/img/goods/PS_50RRWW.jpg'},
    { name : '38 Gold1', img : '/img/goods/PS_38BG1B.jpg'},
    { name : '38 Gold2', img : '/img/goods/PS_38BG2B.jpg'},
    { name : '38 Silver1', img : '/img/goods/PS_38BS1B.jpg'},
    { name : '38 Silver2', img : '/img/goods/PS_38BS2B.jpg'},
    { name : 'Road Stripe', img : '/img/goods/PS_38BROB.jpg'},
    { name : 'French Stripe', img : '/img/goods/PS_38BFRW.jpg'},
    { name : 'Red Seatbelt 50W', img : '/img/goods/PS_50RPW.jpg'},
    { name : 'Red Seatbelt 50B', img : '/img/goods/PS_50RPB.jpg'},
    { name : 'Black Seatbelt 50W', img : '/img/goods/PS_50BPW.jpg'},
    { name : 'Black Seatbelt 50B', img : '/img/goods/PS_50BPB.jpg'},
    { name : 'Black Seatbelt 38W', img : '/img/goods/PS_38BPW.jpg'},
    { name : 'Black Seatbelt 38B', img : '/img/goods/PS_38BPB.jpg'}
  ]
};

