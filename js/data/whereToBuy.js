'use strict';

/*
 * Where to buy 목록
 * region : 지역
 * 분류 : 카테고리 대표 이미지의 URL
 */
var whereToBuy = [
  {
    region : '한국 Korea',
    shops : [
      { name : '가자뮤직', url : 'gajamusic.co.kr' },
      { name : '경은상사', url : 'acousticgt.com' },
      { name : '기타라운지', url : 'guitarlounge.co.kr' },
      { name : '기타뮤즈', url : 'guitarmuse.co.kr' },
      { name : '기타스토리', url : 'gtstory.co.kr' },
      { name : '기타앤사운드', url : 'gtsound.co.kr' },
      { name : '기타존', url : 'guitartrip.co.kr' },
      { name : '기타킹', url : 'guitarking.co.kr' },
      { name : '기타토크', url : 'guitartalk.co.kr' },
      { name : '더기타', url : 'theguitar.co.kr' },
      { name : '드럼몰', url : 'drumall.com' },
      { name : '드럼앤드러머', url : 'drummer.co.kr' },
      { name : '딴따라', url : 'ddanddara.co.kr' },
      { name : '라이딩베이스', url : 'ridinbass.com' },
      { name : '락하우스', url : 'rockhouse.co.kr' },
      { name : '몽키타', url : 'monkeytar.co.kr' },
      { name : '반도몰', url : 'bandomall.co.kr' },
      { name : '버즈비', url : 'buzzbee.co.kr' },
      { name : '산울림악기', url : 'guitaria.co.kr' },
      { name : '샤인기타', url : 'shineguitar.co.kr' },
      { name : '스쿨뮤직', url : 'schoolmusic.co.kr' },
      { name : '아라뮤직', url : 'aramusic.co.kr' },
      { name : '어쿠스틱갤러리', url : 'cafe.naver.com/skymuzic' },
      { name : '엄태창 기타', url : 'omguitar.co.kr' },
      { name : '연세악기', url : 'amosguitar.co.kr' },
      { name : '위락', url : 'werock.co.kr' },
      { name : '윌로스기타', url : 'willowsguitar.com' },
      { name : '통기타이야기', url : 'tongguitar.co.kr' },
      { name : '프리버드', url : 'freebud.co.kr' },
      { name : '피스뮤직', url : 'peacemusic.co.kr' },
      { name : '해성악기사', url : 'haesungmusic.com' }
    ]
  },
  {
    region : '그 외 국가 Other Countries',
    shops : [
      { name : '이베이', url : 'ebay.com' }
    ]
  }
];