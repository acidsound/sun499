﻿'use strict';

var WB0B014PPApp = angular.module('WB0B014PPApp', [])
  .config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'HomeCtrl'
    })
    .when('/wheretobuy', {
      templateUrl: 'views/wheretobuy.html',
      controller: 'WhereToBuyCtrl'
    })
    .when('/contact', {
      templateUrl: 'views/contact.html',
      controller: 'ContactCtrl'
    })
    .when('/category/:category', {
      templateUrl: 'views/category.html',
      controller: 'CategoryCtrl'
    })
    .when('/category/:category/:product', {
      templateUrl: 'views/product.html',
      controller: 'ProductCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);

var HomeCtrl = function ($scope) {
  $scope.list = home;
  $(($('.nav.nav-pills li').removeClass('active'))[0]).addClass('active');
};

var WhereToBuyCtrl = function ($scope) {
  $scope.list = whereToBuy;
  $(($('.nav.nav-pills li').removeClass('active'))[1]).addClass('active');
};

var ContactCtrl = function () {
  $(($('.nav.nav-pills li').removeClass('active'))[2]).addClass('active');
};

var CategoryCtrl = function ($scope, $routeParams) {
  $scope.categoryName = $routeParams.category;
  $scope.category = category[$scope.categoryName];
};

var ProductCtrl = function ($scope, $routeParams) {
  $scope.categoryName = $routeParams.category;
  angular.forEach(category[$scope.categoryName], function(v) {
    if (v.name === $routeParams.product) {
      $scope.content_url = '/contents/'+v.name+'.html';
      $scope.img = v.img;
      $scope.name = v.name;
    }
  });
}